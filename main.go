package main

import (
	"context"
	"dils-backend/api/user"
	"dils-backend/dao"
	"errors"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/mongo"
)

var (
	userDAO     *dao.UserDAO
	factoryDAO  *dao.FactoryDAO
	userService *user.Service
)

func main() {
	env := os.Getenv("ENV")
	if err := godotenv.Load(); err != nil {
		if env != "" && env != "DEPLOY-DEV" {
			log.Println("No .env file found")
			return
		}
	}

	client, err := initDatabase()
	if err != nil {
		log.Fatalf("failed to initialize database, err: %v", err)
		return
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			log.Fatal(err)
		}
	}()

	initServices()

	r := initRoutes()
	port := os.Getenv("PORT")
	log.Println("Running server on port", port)
	if err := http.ListenAndServe(":"+port, r); err != nil {
		log.Fatal(err)
	}
}

func initRoutes() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"status": "ok", "message": "dilis-backend"}`))
	})
	v1 := r.PathPrefix("/api/v1").Subrouter()
	userRouter := v1.PathPrefix("/user").Subrouter()

	userRouter.HandleFunc("/signup", userService.SignupUser).Methods("POST")
	userRouter.HandleFunc("/signin", userService.Signin).Methods("POST")
	userRouter.HandleFunc("/signup/resend-otp", userService.ResendOTP).Methods("POST")
	userRouter.HandleFunc("/confirm", userService.ConfirmAccount).Methods("POST")
	userRouter.HandleFunc("/passwords/request", userService.RequestPasswordReset).Methods("POST")
	userRouter.HandleFunc("/passwords/reset", userService.ResetPassword).Methods("POST")
	userRouter.HandleFunc("/payment-options", userService.AddPaymentOption).Methods("POST")

	return r
}

func initDatabase() (*mongo.Client, error) {
	dbPass := os.Getenv("MONGO_PASS")
	dbname := "dils"

	if dbPass == "" {
		return nil, errors.New("MONGO_SERVER pass not set")
	}

	client, ctx, err := dao.Initialize(dbPass, dbname)
	if err != nil {
		return nil, err
	}

	initCollections(ctx, client)

	return client, nil
}

func initCollections(ctx context.Context, client *mongo.Client) {
	db := client.Database("dils")
	userDAO = dao.NewUserDAO(ctx, db)
	factoryDAO = dao.NewFactoryDAO(ctx, db)
}

func initServices() {
	userService = user.NewUserService(userDAO, factoryDAO)
}
