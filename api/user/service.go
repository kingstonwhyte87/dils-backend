package user

import (
	"context"
	"dils-backend/dao"
	"dils-backend/models"
	"dils-backend/utils"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service represents the User Service
type Service struct {
	dao        *dao.UserDAO
	factoryDAO *dao.FactoryDAO
}

// NewUserService returns a user service object
func NewUserService(dao *dao.UserDAO, factoryDAO *dao.FactoryDAO) *Service {
	payOpts := []string{"bank_payment_option", "paypal_payment_option"}
	for _, opt := range payOpts {
		factoryDAO.Add(opt)
	}

	return &Service{
		dao:        dao,
		factoryDAO: factoryDAO,
	}
}

// SignupUser creates a new user account
func (s *Service) SignupUser(w http.ResponseWriter, r *http.Request) {
	var (
		user models.User
		req  models.CreateUserReq
	)
	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data sent")
		return
	}

	// verify user doesn't exist
	count, err := s.dao.Collection.CountDocuments(context.TODO(), bson.D{
		{Key: "email", Value: req.Email},
	})

	if err != nil {
		log.Printf("failed to retrieve user (%s) err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data sent")
		return
	}

	if count > 0 {
		utils.RespondWithError(w, http.StatusBadRequest, "This user account already exists")
		return
	}

	// update remaining fields
	now := time.Now().Format("01-02-2006 15:04:05")
	user.ID = primitive.NewObjectID()
	user.Username = req.Username
	user.Email = req.Email
	user.Confirmed = false
	user.CreatedAt = now
	user.UpdatedAt = now
	user.PassCode = utils.GenPasscode()

	// hash password
	hash, err := utils.HashPassword(req.Password)
	if err != nil {
		utils.RespondWithError(w, http.StatusInternalServerError, "An Error occurred while processing request")
		return
	}
	user.Password = hash

	// save user to db
	if err := s.dao.Insert(user); err != nil {
		log.Printf("failed to insert new user (%s) err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusInternalServerError, "An Error occurred while processing request")
		return
	}

	go sendVerificationEmail(user)

	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status: "success",
		Code:   http.StatusCreated,
		Data:   user,
	})
}

// ResendOTP resends a new OTP for user signup
func (s *Service) ResendOTP(w http.ResponseWriter, r *http.Request) {
	var req struct {
		Email string `json:"email"`
	}

	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data sent")
		return
	}

	// retrive user
	user, err := s.dao.FindByEmail(req.Email)
	if err != nil {
		log.Printf("failed to retrieve user object for %s, err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusNotFound, "User account not found")
		return
	}

	// regenerate passcode
	user.PassCode = utils.GenPasscode()

	err = s.dao.Update(user)
	if err != nil {
		log.Printf("failed to update user (%s) err, %v", req.Email, err)
		utils.RespondWithError(w, http.StatusBadRequest, "An Error occurred while verifying account")
		return
	}

	go sendVerificationEmail(user)

	utils.RespondWithJSON(w, http.StatusAccepted, utils.Response{
		Status:  "success",
		Code:    http.StatusAccepted,
		Message: "OTP has been resent",
	})
}

// Signin authenticates a user account
func (s *Service) Signin(w http.ResponseWriter, r *http.Request) {
	var (
		req models.LoginReq
	)
	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "This is an invalid request data sent")
		return
	}

	// retrive user
	user, err := s.dao.FindByEmail(req.Email)
	if err != nil {
		log.Printf("failed to retrieve user object for %s, err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusNotFound, "User account not found")
		return
	}

	// validate password
	if !utils.CheckPasswordHash(req.Password, user.Password) {
		utils.RespondWithError(w, http.StatusForbidden, "Invalid credentials")
		return
	}

	// generate jwt
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = jwt.MapClaims{
		"exp":       time.Now().Add(time.Hour * 72).Unix(),
		"email":     user.Email,
		"id":        user.ID,
		"user_name": user.Username,
		"is_active": user.Confirmed,
	}
	signedString, err := token.SignedString([]byte(os.Getenv("SECRET")))
	if err != nil {
		log.Println("error generating token", err)
		utils.RespondWithError(w, http.StatusInternalServerError, "An Error occurred")
		return
	}
	utils.RespondWithJSON(w, http.StatusOK, map[string]interface{}{
		"status": "success",
		"token":  signedString,
	})
}

// ConfirmAccount verifies a user account with pass code generated at signup
func (s *Service) ConfirmAccount(w http.ResponseWriter, r *http.Request) {
	var req models.ConfirmAccountReq

	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}

	// retrieve user account
	user, err := s.dao.FindByEmail(req.Email)
	if err != nil {
		utils.RespondWithError(w, http.StatusNotFound, "No user account found")
		return
	}

	// verify code
	if user.PassCode != req.Code {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid verification code sent")
		return
	}
	user.Confirmed = true

	// update user status
	err = s.dao.Update(user)
	if err != nil {
		log.Printf("failed to update user (%s) err, %v", req.Email, err)
		utils.RespondWithError(w, http.StatusBadRequest, "An Error occurred while verifying account")
		return
	}

	utils.RespondWithJSON(w, http.StatusAccepted, utils.Response{
		Status:  "success",
		Code:    http.StatusAccepted,
		Message: "Account has been activated",
	})
}

// RequestPasswordReset sends a new password reset code
func (s *Service) RequestPasswordReset(w http.ResponseWriter, r *http.Request) {
	var req models.PasswordResetReq

	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}
	code := utils.GenPasscode()

	user, err := s.dao.FindByEmail(req.Email)
	if err != nil {
		log.Printf("failed to retrieve user (%s), err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusNotFound, "User account not found")
		return
	}
	user.PassCode = code

	err = s.dao.Update(user)
	if err != nil {
		log.Printf("failed to update user (%s), err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusNotFound, "Error generating password reset code")
		return
	}

	// mail new code
	sendVerificationEmail(user)
	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status:  "success",
		Code:    http.StatusCreated,
		Message: "Account reset code has been sent!",
	})
}

// ResetPassword resets a user password
func (s *Service) ResetPassword(w http.ResponseWriter, r *http.Request) {
	var req models.PasswordReset

	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}

	// retrieve user
	user, err := s.dao.FindByEmail(req.Email)
	if err != nil {
		log.Printf("failed to retrieve user (%s), err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusNotFound, "User account not found")
		return
	}

	// check passcode validaity
	if req.Code != user.PassCode {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid reset code")
		return
	}

	// hash new password
	hash, err := utils.HashPassword(req.Password)
	if err != nil {
		log.Printf("failed to hash password for %s, err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusBadRequest, "An Error occurred while processing request")
		return
	}

	user.Password = hash

	err = s.dao.Update(user)
	if err != nil {
		log.Printf("failed to update user (%s), err: %v", req.Email, err)
		utils.RespondWithError(w, http.StatusNotFound, "An Error occurred")
		return
	}

	utils.RespondWithJSON(w, http.StatusAccepted, utils.Response{
		Status:  "success",
		Code:    http.StatusAccepted,
		Message: "Password Reset successful!",
	})
}

func sendVerificationEmail(user models.User) {
	email := utils.EmailData{}
	email.EmailTo = user.Email
	email.ContentData = map[string]interface{}{
		"Name": user.Username,
		"Code": user.PassCode,
	}
	email.Template = "activation.html"
	email.Title = "[DILS]: Welcome"
	err := utils.SendEmail(email)
	if err != nil {
		log.Printf("[send_verification]: failed to send mail: %v\n", err)
		return
	}
	log.Println("Verification sent to", user.Email)
}

// AddPaymentOption creates a new payment option for user
func (s *Service) AddPaymentOption(w http.ResponseWriter, r *http.Request) {
	var req models.PaymentOptionReq

	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}

	// TODO: request validation
	var (
		paymentOption interface{}
		selectedOpt   string
	)
	userid, err := primitive.ObjectIDFromHex(req.UserID)
	if err != nil {
		log.Printf("failed to parse userid: %v", err)
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request")
		return
	}
	now := utils.Now()
	switch req.Type {
	case models.Bank:
		selectedOpt = "bank_payment_option"
		paymentOption = models.BankOption{
			ID:            primitive.NewObjectID(),
			UserID:        userid,
			AccountName:   req.AccountName,
			AccountNumber: req.AccountNumber,
			BankName:      req.BankName,
			SortCode:      req.SortCode,
			CreatedAt:     now,
			UpdatedAt:     now,
		}
	case models.PayPal:
		selectedOpt = "paypal_payment_option"
		paymentOption = models.PayPalOption{
			ID:        primitive.NewObjectID(),
			UserID:    userid,
			Email:     req.Email,
			CreatedAt: now,
			UpdatedAt: now,
		}
	}

	if err := s.factoryDAO.Insert(selectedOpt, paymentOption); err != nil {
		log.Printf("failed to insert new paymentOption (%s) err: %v", req.UserID, err)
		utils.RespondWithError(w, http.StatusInternalServerError, "An Error occurred while processing request")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status: "success",
		Code:   http.StatusCreated,
		Data:   paymentOption,
	})
}
