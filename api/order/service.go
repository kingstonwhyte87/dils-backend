package order

import (
	"dils-backend/dao"
	"dils-backend/models"
	"dils-backend/utils"
	"log"
	"net/http"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Service represents the User Service
type Service struct {
	dao *dao.OrderDAO
}

// NewOrderService returns a new order service
func NewOrderService(dao *dao.OrderDAO) *Service {
	return &Service{dao: dao}
}

// CreateSellOrder creates a new sell order
func (s *Service) CreateSellOrder(w http.ResponseWriter, r *http.Request) {
	var (
		order models.Order
		req   models.SellOrderReq
	)
	err := utils.DecodeReq(r, &req)
	if err != nil {
		utils.RespondWithError(w, http.StatusBadRequest, "Invalid request data sent")
		return
	}
	// skip validation of components
	//now := time.Now().Format("01-02-2006 15:04:05")
	order.ID = primitive.NewObjectID()
	order.SellerID = req.SellerID
	order.Amount = req.Amount
	order.ExRate = req.ExRate
	order.WalletID = req.WalletID

	if err := s.dao.Insert(order); err != nil {
		log.Printf("failed to create new sell order: %v", err)
		utils.RespondWithError(w, http.StatusInternalServerError, "An Error occurred while processing request")
		return
	}

	utils.RespondWithJSON(w, http.StatusCreated, utils.Response{
		Status: "success",
		Code:   http.StatusCreated,
		Data:   order,
	})
}
