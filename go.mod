module dils-backend

// +heroku goVersion go1.14
go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	go.mongodb.org/mongo-driver v1.4.5
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
