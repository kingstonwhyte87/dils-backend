package dao

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// FactoryDAO represents a dao for scalfolding and accessing collections
type FactoryDAO struct {
	ctx         context.Context
	db          *mongo.Database
	Collections map[string]*mongo.Collection
}

// NewFactoryDAO returns a new FactoryDAO
func NewFactoryDAO(ctx context.Context, db *mongo.Database) *FactoryDAO {
	return &FactoryDAO{
		ctx:         context.TODO(),
		db:          db,
		Collections: make(map[string]*mongo.Collection),
	}
}

// Add collection to list
func (dao *FactoryDAO) Add(key string) {
	c := dao.db.Collection(key)
	dao.Collections[key] = c
}

// Insert a collection into database
func (dao *FactoryDAO) Insert(key string, obj interface{}) error {
	collection, ok := dao.Collections[key]
	if !ok {
		return errors.New("Invalid collection")
	}
	c, _ := bson.Marshal(obj)
	_, err := collection.InsertOne(dao.ctx, c)
	return err
}
