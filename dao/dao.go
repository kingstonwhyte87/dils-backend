package dao

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// DAO is the base interface for accessing data
type DAO interface {
}

// Initialize a connection
func Initialize(serverPass, dbname string) (*mongo.Client, context.Context, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	clientOpts := options.Client()
	clientOpts.SetAuth(options.Credential{
		AuthMechanism: "SCRAM-SHA-1",
		Username:      "root",
		Password:      serverPass,
	})
	client, err := mongo.Connect(ctx, clientOpts.ApplyURI(fmt.Sprintf("mongodb+srv://root:%s@dils-cluster.0ppxr.mongodb.net/%s?retryWrites=true&w=majority", serverPass, dbname)))
	if err != nil {
		return nil, nil, err
	}

	// ping primary
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return nil, nil, err
	}

	return client, ctx, nil
}
