package dao

import (
	"context"
	"dils-backend/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// OrderDAO represents an order DAO
type OrderDAO struct {
	ctx        context.Context
	db         *mongo.Database
	Collection *mongo.Collection
}

// NewOrderDAO returns a new OrderDAO
func NewOrderDAO(ctx context.Context, db *mongo.Database) *OrderDAO {
	return &OrderDAO{
		ctx:        context.TODO(),
		db:         db,
		Collection: db.Collection("orders"),
	}
}

// Insert an order into database
func (dao *OrderDAO) Insert(order models.Order) error {
	userb, _ := bson.Marshal(order)
	_, err := dao.Collection.InsertOne(dao.ctx, userb)
	return err
}

// Update an existing order
func (dao *OrderDAO) Update(order models.Order) error {
	docID, _ := primitive.ObjectIDFromHex(order.ID.Hex())
	_, err := dao.Collection.UpdateOne(dao.ctx, bson.M{"_id": docID}, bson.M{"$set": order})
	return err
}
