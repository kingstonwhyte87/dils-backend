package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// OrderType ...
type OrderType int

const (
	// Sell Order type
	Sell OrderType = iota
	// Buy Order type
	Buy
)

// Order represents the Orders Collection
type Order struct {
	ID               primitive.ObjectID `json:"id" bson:"_id"`
	Type             int64              `json:"type" bson:"type"`
	SellerID         primitive.ObjectID `json:"seller_id" bson:"seller_id"`
	ExRate           int64              `json:"ex_rate" bson:"ex_rate"`
	Amount           string             `json:"amount" bson:"amount"`
	Currency         string             `json:"currency" bson:"currency"`
	WalletID         primitive.ObjectID `json:"wallet_id" bson:"wallet_id"`
	PaymentOption    int                `json:"payment_option" bson:"payment_option"`
	PaymentOptionID  primitive.ObjectID `json:"payment_option_id" bson:"payment_option_id"`
	WalletPrivateKey string             `json:"wallet_private_key" bson:"wallet_private_key"`
	Note             string             `json:"note" bson:"note"`
	CreatedAt        string             `json:"created_at" bson:"created_at"`
	UpdatedAt        string             `json:"updated_at" bson:"updated_at"`
}

// SellOrderReq ...
type SellOrderReq struct {
	SellerID         primitive.ObjectID `json:"seller_id"`
	ExRate           int64              `json:"ex_rate"`
	Amount           string             `json:"amount"`
	Currency         string             `json:"currency"`
	WalletID         primitive.ObjectID `json:"wallet_id" `
	PaymentOption    int                `json:"payment_option"`
	PaymentOptionID  primitive.ObjectID `json:"payment_option_id"`
	WalletPrivateKey string             `json:"wallet_private_key"`
	Note             string             `json:"note"`
}
