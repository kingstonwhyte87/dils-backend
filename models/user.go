package models

import "go.mongodb.org/mongo-driver/bson/primitive"

// User represents an app user
type User struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Username  string             `json:"username" bson:"user_name"`
	Email     string             `json:"email" bson:"email"`
	Password  string             `json:"-" bson:"password"`
	Confirmed bool               `json:"confirmed" bson:"confirmed"`
	PassCode  int                `json:"-"`
	CreatedAt string             `json:"created_at" bson:"created_at"`
	UpdatedAt string             `json:"updated_at" bson:"updated_at"`
}

// CreateUserReq represents the request model for signup
type CreateUserReq struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

// LoginReq represents the login request
type LoginReq struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// ConfirmAccountReq represents a confirm account request
type ConfirmAccountReq struct {
	Email string `json:"email"`
	Code  int    `json:"code"`
}

// PasswordResetReq ...
type PasswordResetReq struct {
	Email string `json:"email"`
}

// PasswordReset represents a password request request
type PasswordReset struct {
	Email    string `json:"email"`
	Code     int    `json:"code"`
	Password string `json:"password"`
	//ConfirmPassword string `json:"confirm_password"`
}
