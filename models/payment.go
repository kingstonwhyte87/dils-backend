package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type paymentOption int

const (
	// Bank payment option
	Bank paymentOption = iota
	// PayPal payment option
	PayPal
	// Stripe payment option
	Stripe
)

// PaymentOptionReq represents the payment_option create request payload
type PaymentOptionReq struct {
	UserID        string        `json:"user_id"`
	Type          paymentOption `json:"type"`
	Email         string        `json:"email"`
	AccountName   string        `json:"account_name"`
	AccountNumber string        `json:"account_number"`
	BankName      string        `json:"bank_name"`
	SortCode      string        `json:"sort_code"`
}

// PayPalOption ...
type PayPalOption struct {
	ID        primitive.ObjectID `json:"id" bson:"_id"`
	Email     string             `json:"email" bson:"email"`
	UserID    primitive.ObjectID `json:"user_id"`
	CreatedAt string             `json:"created_at" bson:"created_at"`
	UpdatedAt string             `json:"updated_at" bson:"updated_at"`
}

// BankOption ...
type BankOption struct {
	ID            primitive.ObjectID `json:"id" bson:"_id"`
	UserID        primitive.ObjectID `json:"user_id"`
	AccountName   string             `json:"account_name" bson:"account_name"`
	AccountNumber string             `json:"account_number" bson:"account_number"`
	BankName      string             `json:"bank_name" bson:"bank_name"`
	SortCode      string             `json:"sort_code" bson:"sort_code"`
	CreatedAt     string             `json:"created_at" bson:"created_at"`
	UpdatedAt     string             `json:"updated_at" bson:"updated_at"`
}
